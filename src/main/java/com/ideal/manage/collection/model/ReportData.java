package com.ideal.manage.collection.model;

import java.io.Serializable;

/**
 * Desc:    实时数据上报实体
 * Author: Iron
 * CreateDate: 2016-11-14
 * CopyRight: Beijing Yunzong Co., Ltd.
 */
public class ReportData implements Serializable{

    /**
     * 设备Id
     */
    private long equipId;
    /**
     * 阈值Id
     */
    private long thresholdId;
    /**
     * 上传的数据
     */
    private int value;

    public long getEquipId() {
        return equipId;
    }

    public void setEquipId(long equipId) {
        this.equipId = equipId;
    }

    public long getThresholdId() {
        return thresholdId;
    }

    public void setThresholdId(long thresholdId) {
        this.thresholdId = thresholdId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ReportData{" +
                "equipId=" + equipId +
                ", thresholdId=" + thresholdId +
                ", value=" + value +
                '}';
    }
}
