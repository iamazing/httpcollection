package com.ideal.manage.collection.service;


/**
 * Desc:     上报服务
 * CreateDate: 2016-10-12
 * @author Yaming
 */
public interface InboundService {

    /**
     * equip端数据上报接口
     * @param inbound
     * @return
     * @throws Exception
     */
    String handleEquipInbound(String inbound) throws Exception;


}
