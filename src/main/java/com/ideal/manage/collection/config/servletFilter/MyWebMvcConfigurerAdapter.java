package com.ideal.manage.collection.config.servletFilter;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.*;

/**
 * 配置拦截器，使之生效
 * 类似：在spring-mvc.xml配置文件内添加<mvc:interceptor>标签配置拦截器
 */
@Configuration
public class MyWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

    /**
     * InterceptorRegistry 内的addInterceptor需要一个实现HandlerInterceptor接口的拦截器实例，
     * addPathPatterns方法用于设置拦截器的过滤路径规则。
     * @param registry
     */
    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(menuFilter()).addPathPatterns("*//**")
                .excludePathPatterns("/js*//**","/css*//**","/login","/logout","/ace*//**","/error","/test*","/minapp*//**","/project/routePair/appoint_success");
        super.addInterceptors(registry);
    }*/
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        super.configureContentNegotiation(configurer);
        configurer.defaultContentType(MediaType.APPLICATION_JSON).
                mediaType("xml", MediaType.APPLICATION_XML).
                mediaType("json", MediaType.APPLICATION_JSON);
    }

        @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    /**
     * 静态资源处理，比如刚上传的图片加载出来
     * @param registry
     *//*
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //registry.addResourceHandler("/fileInfo*//**").addResourceLocations("file:D:/yaming/workspace/visitor/collection/src/main/resources/static/fileInfo/");
        registry.addResourceHandler("/fileInfo*//**").addResourceLocations("file:/root/visitor/fileInfo/");

    }*/
}
